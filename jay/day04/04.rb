# Read input file
input = File.readlines('04.input')

# Filter out any empty lines
input = input.select {|x| x != "\n"}.collect!{|x| x.chomp}


# put the entries in date order. Can just sort by text
input.sort!

REGEX = /\[(?<year>\d+)-(?<month>\d+)-(?<day>\d+) (?<hour>\d+):(?<minute>\d+)\] (?<text>.*)/

input.collect!{|line| line.match REGEX}

input.collect!{|line| 
  [Time.new(line['year'].to_i, line['month'].to_i, line['day'].to_i, line['hour'].to_i, line['minute'].to_i),
   line['text']]}

guards = Hash.new {|h,k| nil}
current_guard = nil

Sleep = Struct.new(:sleep_start, :sleep_end)

# Build an array of guards and a list of their sleeps
input.each do |line|
  guard_line = line[1].match /Guard #(?<guard_id>\d+) begins shift/ 
  sleep_line = line[1].match /falls asleep/ 
  wake_line = line[1].match /wakes up/ 
  if guard_line
    puts "night is guard #{guard_line['guard_id']}"
    current_guard = guard_line['guard_id']
  end

  if sleep_line
    if guards[current_guard] == nil
        guards[current_guard] = []
    end
    guards[current_guard] << Sleep.new(line[0], nil)
  end

  if wake_line
    guards[current_guard][-1].sleep_end = line[0]
  end
end

def get_single_sleep_time(sleep)
  return (sleep.sleep_end - sleep.sleep_start) / 60
end

def get_total_sleep_time(sleep_list)
  total = 0
  sleep_list.each do |x|
    total += get_single_sleep_time x
  end
  return total
end

sleepiest_guard = guards.max_by{|k, v| get_total_sleep_time v}

def get_sleepy_times(sleep_list)
  sleep_mins = Hash.new
  (0..59).each do |x|
    sleep_mins[x] = 0
  end
  sleep_list.each do |sleep|
    (sleep.sleep_start.min..(sleep.sleep_end.min - 1)).each do |minute|
      sleep_mins[minute] += 1
    end
  end
  return sleep_mins
end

sleepy_times = get_sleepy_times sleepiest_guard[1] 
sleepiest_time = sleepy_times.max_by{|k,v| v}

puts "sleepiest guard is #{sleepiest_guard[0]} and his sleepiest minute is"\
"#{sleepiest_time[0]} with #{sleepiest_time[1]} minutes of sleep time"

puts "Answer is #{sleepiest_guard[0].to_i * sleepiest_time[0]}"


## Part 2
guards2 = guards.collect{|k,v| [k.to_i, get_sleepy_times(v)]}.to_h
guards2 = guards2.collect{|k,v| [k, v.max_by{|k,v| v}]}.to_h

best_guard = guards2.max_by{|k,v| v[1]}
puts "best guard to target is #{best_guard[0]} at minute #{best_guard[1][0]}"
puts "answer is #{best_guard[0] * best_guard[1][0]}"
# Read input file
input = File.readlines('01.input')

# Filter out any empty lines
input = input.select {|x| x != "\n"}

# Turn all input items into integers
input = input.collect {|x| Integer(x)}

# set up some variables
counter = 0
current_frequency = 0
require 'set'
passed_frequencies = Set.new

while not passed_frequencies.include?(current_frequency)
        passed_frequencies.add(current_frequency)
        current_frequency += input[counter % input.length] # Use modulus to loop from start of list
        counter += 1
end

puts "Answer is:"
puts current_frequency


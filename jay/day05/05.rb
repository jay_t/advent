input = File.read('input').chars

def can_fold_letters(a, b)
    return (a != b and a.upcase == b.upcase)
end

def remove_items_from_list(list)
    counter = 1
    while counter < list.length
        if can_fold_letters(list[counter], list[counter - 1])
            # Delete the two characters
            list.delete_at(counter - 1)
            list.delete_at(counter - 1)
            counter = 0 # And start looking again. Set to 0 as it will be made again at the end
        end
        counter += 1
    end
end

remove_items_from_list input

puts "Final length of polymer is #{input.length}"

attempts = input.collect{|x| x.upcase}.uniq

def run_attempt(char, list)
    new_list = Array.new list
    new_list.delete char.upcase
    new_list.delete char.downcase
    remove_items_from_list new_list
    return new_list.length
end

attempts_hash = attempts.collect{|x| [x, run_attempt(x, input)]}.to_h

best = attempts_hash.min

puts "Best letter to remove is #{best[0]} which leaves #{best[1]} polymers in the chain"



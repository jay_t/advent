input = File.readlines("input").collect{|x| x.chomp}

REGEX = /Step (?<prereq>.) must be finished before step (?<id>.) can begin/

input.collect!{|x| x.match(REGEX)}

steps = (input.collect{|x| x[:id]} + input.collect{|x| x[:prereq]}).uniq.sort
step_reqs = Hash.new{|hash, key| hash[key] = []}

input.each do |line|
    step_reqs[line[:id]] << line[:prereq]
end

order = String.new

counter = 0
while steps.length > 0
    current_letter = steps[counter]
    puts "Checking letter #{current_letter}. It has prereqs #{step_reqs[current_letter]}"
    if step_reqs[current_letter].length > 0
        # There are required preceeding steps
        counter += 1
        next
    else
        # This is the one to do
        steps.delete_at counter
        step_reqs.collect{|k,v| v.delete(current_letter)}
        order << current_letter
        counter = 0
    end
end

puts "The solution string is #{order}"


steps = (input.collect{|x| x[:id]} + input.collect{|x| x[:prereq]}).uniq.sort
stepsh = Hash.new
steps.each_with_index{|x, i| stepsh[x] = 61+i}

step_reqs = Hash.new{|hash, key| hash[key] = []}

input.each do |line|
    step_reqs[line[:id]] << line[:prereq]
end

seconds = 0
counter = 0
while stepsh.length > 0
    puts "Running second #{seconds}"
    workers = 5
    stepsh.each do |key, val|
        puts "checking letter #{key}. It has #{val} work left. It has prereqs #{step_reqs[key]}. Currently there are #{workers} workers."
        # if a step has no requirements and there are available workers
        if step_reqs[key].length == 0 and workers > 0
            puts "letter #{key} will get some work"
            stepsh[key] -= 1
            workers -= 1
        end
    end
    to_delete = stepsh.select{|k,v| v == 0}
    to_delete.each do |key,_|
        puts "letter #{key} done. Deleting"
        stepsh.delete(key)
        step_reqs.collect{|k,v| v.delete(key)}
    end
    seconds += 1
end

puts "total seconds taken is #{seconds}"